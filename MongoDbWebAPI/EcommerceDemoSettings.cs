﻿namespace MongoDbWebAPI
{
    public class EcommerceDemoSettings
    {
        public string ConnectionString { get; set; } = null!;
        public string DatabaseName { get; set; } = null!;
        public string CustomerCollectionName { get; set; } = null!;
        public string ProductCollectionName { get; set; } = null!;
        public string ShoppingCartCollectionName { get; set; } = null!;
    }
}
