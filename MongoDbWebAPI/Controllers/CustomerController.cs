﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MongoDbWebAPI.Dtos.Customer;
using MongoDbWebAPI.Models;
using MongoDbWebAPI.Services.Customers;

namespace MongoDbWebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;

        public CustomerController(ICustomerService customerService, IMapper mapper)
        {
            _customerService = customerService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerReadDto>>> GetAll()
        {
            var customers = await _customerService.GetAsync();
            return Ok(_mapper.Map<List<CustomerReadDto>>(customers));
        }

        [HttpGet("id")]
        public async Task<ActionResult<CustomerReadDto>> Get(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest("id is empty");
            var customer = await _customerService.GetAsync(id);
            if (customer is null)
                return NotFound("Customer with id is not found");
            return Ok(_mapper.Map<CustomerReadDto>(customer));
        }

        [HttpPost]
        public async Task<ActionResult<CustomerReadDto>> Create(CustomerCreateDto customerCreateDto)
        {
            if (customerCreateDto is null)
                return BadRequest("model is null");
            var newCustomer = await _customerService.CreateAsync(_mapper.Map<Customer>(customerCreateDto));
            return Ok(_mapper.Map<CustomerReadDto>(newCustomer));
        }

        [HttpPut]
        public async Task<ActionResult<CustomerReadDto>> Update(CustomerUpdateDto customerUpdateDto)
        {
            if (customerUpdateDto is null)
                return BadRequest("model is null");
            if (string.IsNullOrEmpty(customerUpdateDto.Id))
                return BadRequest("id is not found");
            await _customerService.UpdateAsync(customerUpdateDto.Id, _mapper.Map<Customer>(customerUpdateDto));
            return Ok(customerUpdateDto);
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest("id is empty");
            if(await _customerService.GetAsync(id) is null)
                return NotFound($"Customer with id: {id} is not found");
            await _customerService.RemoveAsync(id);
            return Ok($"Data deleted with id: {id}");
        }
    }
}
