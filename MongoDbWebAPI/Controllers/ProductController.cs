﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MongoDbWebAPI.Dtos.Product;
using MongoDbWebAPI.Models;
using MongoDbWebAPI.Services.Products;

namespace MongoDbWebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly IMapper _mapper;

        public ProductController(IProductService productService, IMapper mapper)
        {
            _productService = productService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductReadDto>>> GetAll()
        {
            var products = await _productService.GetAsync();
            return Ok(_mapper.Map<List<ProductReadDto>>(products));
        }

        [HttpGet("id")]
        public async Task<ActionResult<ProductReadDto>> Get(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest("id is empty");
            var product = await _productService.GetAsync(id);
            if (product is null)
                return NotFound("Product with id is not found");
            return Ok(_mapper.Map<ProductReadDto>(product));
        }

        [HttpPost]
        public async Task<ActionResult<ProductReadDto>> Create(ProductCreateDto productCreateDto)
        {
            if (productCreateDto is null)
                return BadRequest("model is null");
            var newProduct = await _productService.CreateAsync(_mapper.Map<Product>(productCreateDto));
            return Ok(_mapper.Map<Product>(newProduct));
        }

        [HttpPut]
        public async Task<ActionResult<ProductReadDto>> Update(ProductUpdateDto productUpdateDto)
        {
            if (productUpdateDto is null)
                return BadRequest("model is null");
            if (string.IsNullOrEmpty(productUpdateDto.Id))
                return BadRequest("id is null");
            await _productService.UpdateAsync(productUpdateDto.Id, _mapper.Map<Product>(productUpdateDto));
            return Ok(productUpdateDto);
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest("id is empty");
            if (await _productService.GetAsync(id) is null)
                return NotFound($"Product with id: {id} is not found");
            await _productService.RemoveAsync(id);
            return Ok($"Data deleted with id: {id}");
        }
    }
}
