﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MongoDbWebAPI.Dtos.Customer;
using MongoDbWebAPI.Dtos.ShoppingCart;
using MongoDbWebAPI.Models;
using MongoDbWebAPI.Services.Customers;
using MongoDbWebAPI.Services.Products;
using MongoDbWebAPI.Services.ShoppingCarts;

namespace MongoDbWebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ShoppingCartController :ControllerBase
    {
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IProductService _productService;
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;

        public ShoppingCartController(IShoppingCartService shoppingCartService, IProductService productService,
            ICustomerService customerService, IMapper mapper)
        {
            _shoppingCartService = shoppingCartService;
            _productService = productService;
            _customerService = customerService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ShoppingCartReadDto>>> GetAll()
        {
            var carts = await _shoppingCartService.GetAsync();
            return Ok(_mapper.Map<List<ShoppingCartReadDto>>(carts));
        }

        [HttpGet("id")]
        public async Task<ActionResult<ShoppingCartReadDto>> Get(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest("id is empty");
            var cart = await _shoppingCartService.GetAsync(id);
            if (cart is null)
                return NotFound("Shopping cart with id is not found");
            return Ok(_mapper.Map<ShoppingCartReadDto>(cart));
        }

        [HttpPost]
        public async Task<ActionResult<ShoppingCartReadDto>> Create(ShoppingCartCreateDto shoppingCartCreateDto)
        {
            if (shoppingCartCreateDto is null)
                return BadRequest("model is null");
            var shoppingCart = new ShoppingCart();
            if (string.IsNullOrEmpty(shoppingCartCreateDto.ProductId))
                return BadRequest("product id is null or empty");
            else
            {
                var product = await _productService.GetAsync(shoppingCartCreateDto.ProductId);
                if (product is null)
                    return NotFound($"Product is not found with product id: {shoppingCartCreateDto.ProductId}");
                shoppingCart.Product = product;
            }

            if (string.IsNullOrEmpty(shoppingCartCreateDto.CustomerId))
                return BadRequest("customer id is null or empty");
            else
            {
                var customer = await _customerService.GetAsync(shoppingCartCreateDto.CustomerId);
                if (customer is null)
                    return NotFound($"Customer is not found with product id: {shoppingCartCreateDto.CustomerId}");
                shoppingCart.Customer = customer;
            }

            if (shoppingCartCreateDto.Quantity <= 0)
                return BadRequest("Quantity can't be zero or less than zero");
            shoppingCart.Quantity = shoppingCartCreateDto.Quantity;
            var newShoppingCart = await _shoppingCartService.CreateAsync(shoppingCart);
            return Ok(_mapper.Map<ShoppingCartReadDto>(newShoppingCart));
        }

        [HttpPut]
        public async Task<ActionResult<ShoppingCartReadDto>> Update(ShoppingCartUpdateDto shoppingCartUpdateDto)
        {
            if (shoppingCartUpdateDto is null)
                return BadRequest("model is null");
            if (string.IsNullOrEmpty(shoppingCartUpdateDto.Id))
                return BadRequest("id is not found");
            var shoppingCart = new ShoppingCart();
            if (string.IsNullOrEmpty(shoppingCartUpdateDto.ProductId))
                return BadRequest("product id is null or empty");
            else
            {
                var product = await _productService.GetAsync(shoppingCartUpdateDto.ProductId);
                if (product is null)
                    return NotFound($"Product is not found with product id: {shoppingCartUpdateDto.ProductId}");
                shoppingCart.Product = product;
            }

            if (string.IsNullOrEmpty(shoppingCartUpdateDto.CustomerId))
                return BadRequest("customer id is null or empty");
            else
            {
                var customer = await _customerService.GetAsync(shoppingCartUpdateDto.CustomerId);
                if (customer is null)
                    return NotFound($"Customer is not found with product id: {shoppingCartUpdateDto.CustomerId}");
                shoppingCart.Customer = customer;
            }

            if (shoppingCartUpdateDto.Quantity <= 0)
                return BadRequest("Quantity can't be zero or less than zero");
            shoppingCart.Quantity = shoppingCartUpdateDto.Quantity;
            await _shoppingCartService.UpdateAsync(shoppingCartUpdateDto.Id, shoppingCart);
            return Ok(shoppingCartUpdateDto);
        }

        [HttpDelete]
        public async Task<ActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
                return BadRequest("id is empty");
            if (await _shoppingCartService.GetAsync(id) is null)
                return NotFound($"Shopping Cart with id: {id} is not found");
            await _shoppingCartService.RemoveAsync(id);
            return Ok($"Data deleted with id: {id}");
        }
    }
}
