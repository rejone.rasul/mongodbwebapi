﻿using MongoDbWebAPI.Models;

namespace MongoDbWebAPI.Services.Products
{
    public interface IProductService
    {
        Task<List<Product>> GetAsync();
        Task<Product?> GetAsync(string id);
        Task<Product> CreateAsync(Product newCustomer);
        Task UpdateAsync(string id, Product updatedCustomer);
        Task RemoveAsync(string id);
    }
}
