﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDbWebAPI.Models;

namespace MongoDbWebAPI.Services.Products
{
    public class ProductService : IProductService
    {
        private readonly IMongoCollection<Product> _productCollection;
        private readonly DbClient _dbClient;

        public ProductService(IOptions<EcommerceDemoSettings> databaseSettings, DbClient dbClient)
        {
            _dbClient = dbClient;
            _productCollection = _dbClient.GetCollection<Product>(databaseSettings.Value.ProductCollectionName);
        }
        public async Task<List<Product>> GetAsync() =>
            await _productCollection.Find(_ => true).ToListAsync();

        public async Task<Product?> GetAsync(string id) =>
            await _productCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

        public async Task<Product> CreateAsync(Product newProduct)
        {
            await _productCollection.InsertOneAsync(newProduct);
            return newProduct;
        }

        public async Task UpdateAsync(string id, Product updatedProduct) =>
            await _productCollection.ReplaceOneAsync(x => x.Id == id, updatedProduct);

        public async Task RemoveAsync(string id) =>
            await _productCollection.DeleteOneAsync(x => x.Id == id);
    }
}
