﻿using MongoDbWebAPI.Models;

namespace MongoDbWebAPI.Services.Customers
{
    public interface ICustomerService
    {
        Task<List<Customer>> GetAsync();
        Task<Customer?> GetAsync(string id);
        Task<Customer> CreateAsync(Customer newCustomer);
        Task UpdateAsync(string id, Customer updatedCustomer);
        Task RemoveAsync(string id);
    }
}
