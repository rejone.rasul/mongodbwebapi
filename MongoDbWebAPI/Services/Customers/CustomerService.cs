﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDbWebAPI.Models;

namespace MongoDbWebAPI.Services.Customers
{
    public class CustomerService : ICustomerService
    {
        private readonly IMongoCollection<Customer> _customersCollection;
        private readonly DbClient _dbClient;

        public CustomerService(IOptions<EcommerceDemoSettings> databaseSettings, DbClient dbClient)
        {
            _dbClient = dbClient;
            _customersCollection = _dbClient.GetCollection<Customer>(databaseSettings.Value.CustomerCollectionName);
        }

        public async Task<List<Customer>> GetAsync() =>
            await _customersCollection.Find(_ => true).ToListAsync();

        public async Task<Customer?> GetAsync(string id) =>
            await _customersCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

        public async Task<Customer> CreateAsync(Customer newCustomer)
        {
            await _customersCollection.InsertOneAsync(newCustomer);
            return newCustomer;
        }

        public async Task UpdateAsync(string id, Customer updatedCustomer) =>
            await _customersCollection.ReplaceOneAsync(x => x.Id == id, updatedCustomer);

        public async Task RemoveAsync(string id) =>
            await _customersCollection.DeleteOneAsync(x => x.Id == id);
    }
}
