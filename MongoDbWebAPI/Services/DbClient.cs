﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace MongoDbWebAPI.Services
{
    public class DbClient
    {
        //private readonly IMongoCollection<Customer> _customersCollection;
        private readonly IMongoDatabase _mongoDatabase;
        public DbClient(IOptions<EcommerceDemoSettings> databaseSettings)
        {
            var mongoClient = new MongoClient(
            databaseSettings.Value.ConnectionString);

            _mongoDatabase = mongoClient.GetDatabase(
                databaseSettings.Value.DatabaseName);
        }

        public IMongoCollection<T> GetCollection<T>(string collectionName)
        {
            return _mongoDatabase.GetCollection<T>(
                collectionName);
        }
    }
}
