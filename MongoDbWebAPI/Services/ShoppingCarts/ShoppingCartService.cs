﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDbWebAPI.Models;

namespace MongoDbWebAPI.Services.ShoppingCarts
{
    public class ShoppingCartService : IShoppingCartService
    {
        private readonly IMongoCollection<ShoppingCart> _shoppingCartCollection;
        private readonly DbClient _dbClient;

        public ShoppingCartService(IOptions<EcommerceDemoSettings> databaseSettings, DbClient dbClient)
        {
            _dbClient = dbClient;
            _shoppingCartCollection = _dbClient.GetCollection<ShoppingCart>(databaseSettings.Value.ShoppingCartCollectionName);
        }
        public async Task<List<ShoppingCart>> GetAsync() =>
            await _shoppingCartCollection.Find(_ => true).ToListAsync();

        public async Task<ShoppingCart?> GetAsync(string id) =>
            await _shoppingCartCollection.Find(x => x.Id == id).FirstOrDefaultAsync();

        public async Task<ShoppingCart> CreateAsync(ShoppingCart shoppingCart)
        {
            await _shoppingCartCollection.InsertOneAsync(shoppingCart);
            return shoppingCart;
        }

        public async Task UpdateAsync(string id, ShoppingCart updatedCart) =>
            await _shoppingCartCollection.ReplaceOneAsync(x => x.Id == id, updatedCart);

        public async Task RemoveAsync(string id) =>
            await _shoppingCartCollection.DeleteOneAsync(x => x.Id == id);
    }
}
