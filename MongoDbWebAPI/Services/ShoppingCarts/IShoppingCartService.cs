﻿using MongoDbWebAPI.Models;

namespace MongoDbWebAPI.Services.ShoppingCarts
{
    public interface IShoppingCartService
    {
        Task<List<ShoppingCart>> GetAsync();
        Task<ShoppingCart?> GetAsync(string id);
        Task<ShoppingCart> CreateAsync(ShoppingCart newCart);
        Task UpdateAsync(string id, ShoppingCart updatedCart);
        Task RemoveAsync(string id);
    }
}
