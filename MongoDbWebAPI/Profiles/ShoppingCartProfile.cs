﻿using AutoMapper;
using MongoDbWebAPI.Dtos.Product;
using MongoDbWebAPI.Dtos.ShoppingCart;
using MongoDbWebAPI.Models;

namespace MongoDbWebAPI.Profiles
{
    public class ShoppingCartProfile : Profile
    {
        public ShoppingCartProfile()
        {
            // Source -> Destination
            CreateMap<ShoppingCart, ShoppingCartReadDto>()
                .ForMember(dest => dest.Product, opt => opt.MapFrom(src => src.Product))
                .ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer));
            CreateMap<ShoppingCartCreateDto, ShoppingCart>();
            CreateMap<ShoppingCartUpdateDto, ShoppingCart>();
        }
    }
}
