﻿using AutoMapper;
using MongoDbWebAPI.Dtos.Customer;
using MongoDbWebAPI.Models;

namespace MongoDbWebAPI.Profiles
{
    public class CustomerProfile : Profile
    {
        public CustomerProfile()
        {
            // Source -> Destination
            CreateMap<Customer, CustomerReadDto>();
            CreateMap<CustomerCreateDto, Customer>();
            CreateMap<CustomerUpdateDto, Customer>();
        }
    }
}
