﻿using AutoMapper;
using MongoDbWebAPI.Dtos.Product;
using MongoDbWebAPI.Models;

namespace MongoDbWebAPI.Profiles
{
    public class ProductProfile : Profile
    {
        public ProductProfile()
        {
            // Source -> Destination
            CreateMap<Product, ProductReadDto>();
            CreateMap<ProductCreateDto, Product>();
            CreateMap<ProductUpdateDto, Product>();
        }
    }
}
