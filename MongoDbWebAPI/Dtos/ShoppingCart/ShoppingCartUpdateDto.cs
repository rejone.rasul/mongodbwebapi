﻿namespace MongoDbWebAPI.Dtos.ShoppingCart
{
    public class ShoppingCartUpdateDto
    {
        public string Id { get; set; }
        public string CustomerId { get; set; }
        public string ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
