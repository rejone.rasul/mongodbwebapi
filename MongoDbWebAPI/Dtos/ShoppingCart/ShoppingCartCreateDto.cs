﻿namespace MongoDbWebAPI.Dtos.ShoppingCart
{
    public class ShoppingCartCreateDto
    {
        public string CustomerId { get; set; }
        public string ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
