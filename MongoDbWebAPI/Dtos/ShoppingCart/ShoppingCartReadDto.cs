﻿using MongoDbWebAPI.Dtos.Customer;
using MongoDbWebAPI.Dtos.Product;

namespace MongoDbWebAPI.Dtos.ShoppingCart
{
    public class ShoppingCartReadDto
    {
        public string? Id { get; set; }
        public CustomerReadDto Customer { get; set; }
        public ProductReadDto Product { get; set; }
        public int Quantity { get; set; }
    }
}
