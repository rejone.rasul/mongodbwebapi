﻿namespace MongoDbWebAPI.Dtos.Customer
{
    public class CustomerCreateDto
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
