﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace MongoDbWebAPI.Models
{
    public class Customer
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }
        [BsonElement("Email")]
        public string Email { get; set; } = string.Empty;
        [BsonElement("FirstName")]
        public string FirstName { get; set; } = string.Empty;
        [BsonElement("LastName")]
        public string LastName { get; set; } = string.Empty;
    }
}
